INSERT INTO problems (id, name) VALUES (1, 'Problem 1'), (2, 'Problem 2'),(3, 'Problem 3'), (4, 'Problem 4'),(5, 'Problem 5'), (6, 'Problem 6');
INSERT INTO skills (id, name) VALUES (1, 'add-decimals'),(2, 'multiply-decimals'),(3, 'add-fractions'),(4, 'multiply-fractions');
INSERT INTO problem_skills (problem_id, skill_id) VALUES (1, 1), (2, 1), (2, 2),(3, 3), (4, 3), (4, 4), (5, 2), (5, 4), (6, 3), (6, 1);
INSERT INTo students (id, name) values (1, 'Pablo Contreras');