package com.example.system.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Data
@Table(name = "skills")
public class Skill implements Serializable {
    @Id
    private Long id;

    @Column(unique = true)
    private String name;
}
