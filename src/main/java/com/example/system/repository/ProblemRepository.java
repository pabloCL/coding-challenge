package com.example.system.repository;

import com.example.system.entity.Problem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProblemRepository extends JpaRepository<Problem, Long> {
    @Query(value = "select p.id as id, p.name as name from problems p join problem_skills ps on p.id = ps.problem_id where ps.skill_id =?1", nativeQuery = true)
    List<Problem> findAllBySkill(Long skill);
}
