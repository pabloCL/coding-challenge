package com.example.system.api;

import com.example.system.dto.RequestDTO;
import com.example.system.entity.Problem;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("api/problem")
public interface ProblemAPI extends GenericAPI<Problem> {

    @PostMapping("/student/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> findProblem(@PathVariable("id") Long studentId, @RequestBody @Valid RequestDTO request);

}
