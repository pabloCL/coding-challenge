package com.example.system.controller;

import com.example.system.api.ProblemAPI;
import com.example.system.dto.RequestDTO;
import com.example.system.entity.Problem;
import com.example.system.service.IProblemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class ProblemController implements ProblemAPI {

    private final IProblemService problemService;

    @Autowired
    public ProblemController(IProblemService problemService) {
        this.problemService = problemService;
    }

    @Override
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(this.problemService.findAll());
    }

    @Override
    public ResponseEntity<?> save(@Valid Problem entity) {
        return ResponseEntity.ok(this.problemService.save(entity));
    }

    @Override
    public ResponseEntity<?> update(Long id, Problem entity) {
        return null;
    }

    @Override
    public ResponseEntity<?> delete(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<?> findById(Long id) {
        return ResponseEntity.ok(this.problemService.findById(id));
    }

    @Override
    public ResponseEntity<?> findProblem(Long studentId, RequestDTO request) {
        return ResponseEntity.ok(this.problemService.findProblemByStudentScores(studentId, request.getScores()));
    }
}
