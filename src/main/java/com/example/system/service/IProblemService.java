package com.example.system.service;

import com.example.system.dto.ScoreDTO;
import com.example.system.entity.Problem;
import com.example.system.service.impl.ICRUD;

import java.util.List;

public interface IProblemService extends ICRUD<Problem> {
    List<Problem> findProblemByStudentScores(Long studentId, List<ScoreDTO> scores);
}
