package com.example.system.service.impl;

import com.example.system.entity.Score;
import com.example.system.repository.ScoreRepository;
import com.example.system.service.IScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScoreServiceImpl implements IScoreService {

    private final ScoreRepository scoreRepository;

    @Autowired
    public ScoreServiceImpl(ScoreRepository scoreRepository) {
        this.scoreRepository = scoreRepository;
    }


    @Override
    public List<Score> findAll() {
        return this.scoreRepository.findAll();
    }

    @Override
    public Score save(Score score) {
        return this.scoreRepository.save(score);
    }

    @Override
    public Score findById(Long id) {
        return this.scoreRepository.findById(id).orElse(null);
    }

    @Override
    public void deleteById(Long id) {
        this.scoreRepository.deleteById(id);
    }
}
