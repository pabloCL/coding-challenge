package com.example.system.service.impl;

import com.example.system.entity.Skill;
import com.example.system.repository.SkillRepository;
import com.example.system.service.ISkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class SkillServiceImpl implements ISkillService {

    private final SkillRepository skillRepository;

    @Autowired
    public SkillServiceImpl(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }


    @Override
    public List<Skill> findAll() {
        return this.skillRepository.findAll();
    }

    @Override
    public Skill save(Skill skill) {
        return this.skillRepository.save(skill);
    }

    @Override
    public Skill findById(Long id) {
        return this.skillRepository.findById(id).orElse(null);
    }

    @Override
    public void deleteById(Long id) {
        this.skillRepository.deleteById(id);
    }
}
