package com.example.system.service.impl;

import com.example.system.dto.ScoreDTO;
import com.example.system.entity.Problem;
import com.example.system.entity.Score;
import com.example.system.entity.Student;
import com.example.system.repository.ProblemRepository;
import com.example.system.repository.ScoreRepository;
import com.example.system.repository.SkillRepository;
import com.example.system.repository.StudentRepository;
import com.example.system.service.IProblemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class ProblemServiceImpl implements IProblemService {

    private final ProblemRepository problemRepository;
    private final StudentRepository studentRepository;
    private final SkillRepository skillRepository;
    private final ScoreRepository scoreRepository;

    @Autowired
    public ProblemServiceImpl(ProblemRepository problemRepository, StudentRepository studentRepository, SkillRepository skillRepository, ScoreRepository scoreRepository) {
        this.problemRepository = problemRepository;
        this.studentRepository = studentRepository;
        this.skillRepository = skillRepository;
        this.scoreRepository = scoreRepository;
    }


    @Override
    public List<Problem> findAll() {
        return this.problemRepository.findAll();
    }

    @Override
    public Problem save(Problem problem) {
        return this.problemRepository.save(problem);
    }

    @Override
    public Problem findById(Long id) {
        return this.problemRepository.findById(id).orElse(null);
    }

    @Override
    public void deleteById(Long id) {
        this.problemRepository.deleteById(id);
    }

    @Override
    public List<Problem> findProblemByStudentScores(Long studentId, List<ScoreDTO> scores) {
        //Finding Student By Id
        Optional<Student> studentOptional = this.studentRepository.findById(studentId);
        Student student = null;
        List<Score> scoreList = new ArrayList<>();
        //Check if present
        if(studentOptional.isPresent()){
            student = studentOptional.get();
            Student finalStudent = student;
            scores.forEach(scoreDTO -> {
                Score score = new Score();
                score.setValue(scoreDTO.getValue());
                score.setSkill(scoreDTO.getSkill());
                score.setStudent(finalStudent);
                scoreList.add(score);
            });
            //Save the scores
            this.scoreRepository.saveAll(scoreList);
            //Get min score
            Score score = scoreList.stream()
                .min(Comparator.comparing(Score::getValue))
                .orElseThrow(NoSuchElementException::new);
            if(score.getValue() < 95){
                return this.problemRepository.findAllBySkill(score.getSkill().getId());
            }
        }
        return null;
    }
}
