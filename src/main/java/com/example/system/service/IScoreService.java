package com.example.system.service;

import com.example.system.entity.Score;
import com.example.system.service.impl.ICRUD;

public interface IScoreService extends ICRUD<Score> {
}
