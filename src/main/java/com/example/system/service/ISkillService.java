package com.example.system.service;

import com.example.system.entity.Skill;
import com.example.system.service.impl.ICRUD;

public interface ISkillService extends ICRUD<Skill> {
}
