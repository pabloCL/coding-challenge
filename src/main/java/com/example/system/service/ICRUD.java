package com.example.system.service.impl;

import java.io.Serializable;
import java.util.List;

public interface ICRUD<T extends Serializable> {

    List<T> findAll();

    T save(T t);

    T findById(Long id);

    void deleteById(Long id);
}
