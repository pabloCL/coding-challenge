package com.example.system.service;

import com.example.system.entity.Student;
import com.example.system.service.impl.ICRUD;

public interface IStudentService extends ICRUD<Student> {
}
