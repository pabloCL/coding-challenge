package com.example.system.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor
public class RequestDTO {
    private List<ScoreDTO> scores;
}
