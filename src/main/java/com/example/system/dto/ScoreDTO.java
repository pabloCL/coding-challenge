package com.example.system.dto;

import com.example.system.entity.Skill;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@RequiredArgsConstructor
public class ScoreDTO implements Serializable {
    @NotNull(message = "Value cannot be null")
    private Skill skill;
    @Min(value = 0, message = "Minimum score is 0")
    @Max(value = 100, message = "Maximum score is 100")
    private Integer value;
}
