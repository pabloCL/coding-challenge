package com.example.system.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@Data
@RequiredArgsConstructor
public class ProblemDTO implements Serializable {
    private String name;
}
